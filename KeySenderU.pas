unit KeySenderU;

interface

uses
  System.Classes, System.SysUtils, KeySenderLogU,
  Winapi.Windows, Winapi.PsAPI; // added

type
  TKeyOriginatorType = (Host, Target, Other);

  TBeforeSendKeysEvent = procedure (Keys: string; var Cancel: Boolean) of object;
  TAfterSendKeysEvent = procedure (Keys: string) of object;

  PKBDLLHOOKSTRUCT = ^KBDLLHOOKSTRUCT;
  KBDLLHOOKSTRUCT = record
    vkCode: DWORD;
    scanCode: DWORD;
    flags: DWORD;
    time: DWORD;
    dwExtraInfo: ULONG_PTR;
  end;

  TCatchedKeyDownEvent = procedure (WM: WPARAM; Key: PKBDLLHOOKSTRUCT; ch: Char; var CancelKeyRedirection, EatKeyUp,
    RedirectToKeySenderHost: Boolean; ShiftDown, CtrlDown, AltDown: Boolean; KeyOriginatorType: TKeyOriginatorType) of object;
  TAfterKeyRedirectionEvent = procedure (WM: WPARAM; Key: PKBDLLHOOKSTRUCT; ch: Char; ShiftDown, CtrlDown, AltDown: Boolean) of object;
  TPausedChangeEvent = procedure (Paused: Boolean) of object;
  TCatchingKeyDownPausedChangeEvent = procedure (Paused: Boolean) of object;

  TKeySender = class(TComponent)
  private
    FLibChecked: boolean;
    FLibLoaded: boolean;
    FHookInstalled: boolean;
    FEnabled: boolean;
    FTargetExe: string;
    FExclusionExes: TStringList;
    FKeySenderLog: TKeySenderLog;
    FTargetTitle: string;
    FExclusionTitles: TStringList;
    FIAmExclusion: boolean;
    FExclusionClasses: TStringList;
    FTargetClass: string;
    FDebugEnabled: boolean;
    FBeforeSendKeys: TBeforeSendKeysEvent;
    FAfterSendKeys: TAfterSendKeysEvent;
    FCatchedKeyDown: TCatchedKeyDownEvent;
    FAfterKeyRedirection: TAfterKeyRedirectionEvent;
    FCatchingKeyDownEnabled: Boolean;
    FPaused: Boolean;
    FCatchingKeyDownPaused: Boolean;
    FPausedChange: TPausedChangeEvent;
    FCatchingKeyDownPausedChange: TCatchingKeyDownPausedChangeEvent;
    procedure SetEnabled(const Value: boolean);
    procedure SetEnabledRuntime;
    procedure SetCatchingKeyDownEnabled(const Value: Boolean);
    procedure SetCatchingKeyDownEnabledRuntime;
    function CheckLib: boolean;
    procedure log(const s:string);
    procedure SetExclusionExes(const Value: TStringList);
    procedure SetExclusionTitles(const Value: TStringList);
    procedure SetTargetExe(const Value: string);
    procedure SetTargetTitle(const Value: string);
    procedure SetExclusionClasses(const Value: TStringList);
    procedure SetTargetClass(const Value: string);
    procedure deb(const s:string);
    procedure PushKeys(Keys: string; ShiftOn: Boolean; ControlOn: Boolean; AltOn: Boolean);
    procedure PushFnKey(KeyCode: String; ShiftOn, ControlOn, AltOn: Boolean);
    procedure SendKey(ch: Char; ShiftOn: Boolean; ControlOn: Boolean; AltOn: Boolean);
    procedure SendFnKey(VKey: Byte; Shift, Control, Alt: Boolean);
    procedure SendKeyPress(VKey: Word; Ch: Char = #0);
    procedure SendSCAKeyPress(SCAKey, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
    procedure Send2SCAKeyPress(SCAKey1, SCAKey2, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
    procedure Send3SCAKeyPress(SCAKey1, SCAKey2, SCAKey3, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
    procedure DoBeforeSendKeys(Keys: string; var Cancel: Boolean);
    procedure DoAfterSendKeys(Keys: string);
    procedure SetPaused(const Value: Boolean);
    procedure SetCatchingKeyDownPaused(const Value: Boolean);
    procedure SetDebugEnabled(const Value: boolean);
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ApplyDllSettings: Boolean;
    function GetTargetHandle: HWND;
    procedure SendKeys(Keys: String);
  published
    property Enabled: boolean read FEnabled write SetEnabled;
    property Paused: Boolean read FPaused write SetPaused;
    property TargetExe: string read FTargetExe write SetTargetExe;
    property TargetTitle: string read FTargetTitle write SetTargetTitle;
    property TargetClass: string read FTargetClass write SetTargetClass;
    property ExclusionExes: TStringList read FExclusionExes write SetExclusionExes;
    property ExclusionTitles: TStringList read FExclusionTitles write SetExclusionTitles;
    property ExclusionClasses: TStringList read FExclusionClasses write SetExclusionClasses;
    property IAmExclusion: boolean read FIAmExclusion write FIAmExclusion;
    property KeySenderLog: TKeySenderLog read FKeySenderLog write FKeySenderLog;
    property DebugEnabled: boolean read FDebugEnabled write SetDebugEnabled;
    property BeforeSendKeys: TBeforeSendKeysEvent read FBeforeSendKeys write FBeforeSendKeys;
    property AfterSendKeys: TAfterSendKeysEvent read FAfterSendKeys write FAfterSendKeys;
    property CatchedKeyDown: TCatchedKeyDownEvent read FCatchedKeyDown write FCatchedKeyDown;
    property AfterKeyRedirection: TAfterKeyRedirectionEvent read FAfterKeyRedirection write FAfterKeyRedirection;
    property CatchingKeyDownEnabled: Boolean read FCatchingKeyDownEnabled write SetCatchingKeyDownEnabled;
    property CatchingKeyDownPaused: Boolean read FCatchingKeyDownPaused write SetCatchingKeyDownPaused;
    property PausedChange: TPausedChangeEvent read FPausedChange write FPausedChange;
    property CatchingKeyDownPausedChange: TCatchingKeyDownPausedChangeEvent read FCatchingKeyDownPausedChange
      write FCatchingKeyDownPausedChange;
  end;

procedure Register;

implementation

uses //Winapi.Windows,
  Winapi.Messages, Vcl.Controls;

const
  keydll = 'KeySenderDll.dll';

type
  TInstallHook = function: boolean;
  TUninstallHook = function: boolean;
  //TApplyData = procedure(hwMainWnd, hwLogWnd: HWND; const namTargTit, namTargExe, namExclTit, namExclExe: string);
  //TApplyData = procedure(hwMainWnd, hwLogWnd: HWND; const namTargTit, namTargExe, namTargClass, namExclTit, namExclExe, namExclClass: string; DebugEnabled: boolean);
  TApplyData = procedure(hwMainWnd, hwLogWnd: HWND; const namTargTit, namTargExe, namTargClass, namExclTit, namExclExe, namExclClass: string;
    DebugEnabled: boolean; CatchedKeyDownproc: TCatchedKeyDownEvent; afterKeyRedirectionproc: TAfterKeyRedirectionEvent;
    Paused, CatchedKeyDownPaused: Boolean);

var
  InstallHook: TInstallHook=nil;
  UninstallHook: TUninstallHook=nil;
  ApplyData: TApplyData=nil;
  hlib: HMODULE=0;

{ TKeySender }

procedure TKeySender.SendKeyPress(VKey: Word; Ch: Char = #0);
var
  inpArr: array [0..1] of TInput;
begin
  ZeroMemory(@inpArr,sizeof(inpArr));
  inpArr[0].Itype := INPUT_KEYBOARD;
  if (VKey <> 255) then
    inpArr[0].ki.wVk := VKey
  else
  begin
    inpArr[0].ki.wScan := Ord(Ch);
    inpArr[0].ki.dwFlags := KEYEVENTF_UNICODE;
  end;

  inpArr[1].Itype := INPUT_KEYBOARD;
  if (VKey <> 255) then
  begin
    inpArr[1].ki.wVk := VKey;
    inpArr[1].ki.dwFlags := KEYEVENTF_KEYUP;
  end
  else
  begin
    inpArr[1].ki.wScan := Ord(Ch);
    inpArr[1].ki.dwFlags := KEYEVENTF_UNICODE or KEYEVENTF_KEYUP;
  end;

  SendInput(2,inpArr[0],sizeof(TInput));
end;

procedure TKeySender.SendSCAKeyPress(SCAKey, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
var
  inpArr: array [0..3] of TInput;
begin
  ZeroMemory(@inpArr,sizeof(inpArr));

  inpArr[0].Itype := INPUT_KEYBOARD;
  inpArr[0].ki.wVk := SCAKey;

  inpArr[1].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[1].ki.wScan := ScanCode;
    inpArr[1].ki.dwFlags := KEYEVENTF_SCANCODE;
  end else
  begin
    if (VKey <> 255) then
      inpArr[1].ki.wVk := VKey
    else
    begin
      inpArr[1].ki.wScan := Ord(Ch);
      inpArr[1].ki.dwFlags := KEYEVENTF_UNICODE;
    end;
  end;

  inpArr[2].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[2].ki.wScan := ScanCode;
    inpArr[2].ki.dwFlags := KEYEVENTF_SCANCODE or KEYEVENTF_KEYUP;
  end else
  begin
    if (VKey <> 255) then
    begin
      inpArr[2].ki.wVk := VKey;
      inpArr[2].ki.dwFlags := KEYEVENTF_KEYUP;
    end
    else
    begin
      inpArr[2].ki.wScan := Ord(Ch);
      inpArr[2].ki.dwFlags := KEYEVENTF_UNICODE or KEYEVENTF_KEYUP;
    end;
  end;

  inpArr[3].Itype := INPUT_KEYBOARD;
  inpArr[3].ki.wVk := SCAKey;
  inpArr[3].ki.dwFlags := KEYEVENTF_KEYUP;

  SendInput(4,inpArr[0],sizeof(TInput));
end;

procedure TKeySender.Send2SCAKeyPress(SCAKey1, SCAKey2, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
var
  inpArr: array [0..5] of TInput;
begin
  ZeroMemory(@inpArr,sizeof(inpArr));

  inpArr[0].Itype := INPUT_KEYBOARD;
  inpArr[0].ki.wVk := SCAKey1;

  inpArr[1].Itype := INPUT_KEYBOARD;
  inpArr[1].ki.wVk := SCAKey2;

  inpArr[2].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[2].ki.wScan := ScanCode;
    inpArr[2].ki.dwFlags := KEYEVENTF_SCANCODE;
  end else
  begin
    if (VKey <> 255) then
      inpArr[2].ki.wVk := VKey
    else
    begin
      inpArr[2].ki.wScan := Ord(Ch);
      inpArr[2].ki.dwFlags := KEYEVENTF_UNICODE;
    end;
  end;

  inpArr[3].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[3].ki.wScan := ScanCode;
    inpArr[3].ki.dwFlags := KEYEVENTF_SCANCODE or KEYEVENTF_KEYUP;
  end else
    begin
    if (VKey <> 255) then
    begin
      inpArr[3].ki.wVk := VKey;
      inpArr[3].ki.dwFlags := KEYEVENTF_KEYUP;
    end
    else
    begin
      inpArr[3].ki.wScan := Ord(Ch);
      inpArr[3].ki.dwFlags := KEYEVENTF_UNICODE or KEYEVENTF_KEYUP;
    end;
  end;

  inpArr[4].Itype := INPUT_KEYBOARD;
  inpArr[4].ki.wVk := SCAKey2;
  inpArr[4].ki.dwFlags := KEYEVENTF_KEYUP;

  inpArr[5].Itype := INPUT_KEYBOARD;
  inpArr[5].ki.wVk := SCAKey1;
  inpArr[5].ki.dwFlags := KEYEVENTF_KEYUP;

  SendInput(6,inpArr[0],sizeof(TInput));
end;

procedure TKeySender.Send3SCAKeyPress(SCAKey1, SCAKey2, SCAKey3, VKey: Word; ScanCode: Word = 0; Ch: Char = #0);
var
  inpArr: array [0..7] of TInput;
begin
  ZeroMemory(@inpArr,sizeof(inpArr));

  inpArr[0].Itype := INPUT_KEYBOARD;
  inpArr[0].ki.wVk := SCAKey1;

  inpArr[1].Itype := INPUT_KEYBOARD;
  inpArr[1].ki.wVk := SCAKey2;

  inpArr[2].Itype := INPUT_KEYBOARD;
  inpArr[2].ki.wVk := SCAKey3;

  inpArr[3].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[3].ki.wScan := ScanCode;
    inpArr[3].ki.dwFlags := KEYEVENTF_SCANCODE;
  end else
  begin
    if (VKey <> 255) then
      inpArr[3].ki.wVk := VKey
    else
    begin
      inpArr[3].ki.wScan := Ord(Ch);
      inpArr[3].ki.dwFlags := KEYEVENTF_UNICODE;
    end;
  end;

  inpArr[4].Itype := INPUT_KEYBOARD;
  if ScanCode <> 0 then
  begin
    inpArr[4].ki.wScan := ScanCode;
    inpArr[4].ki.dwFlags := KEYEVENTF_SCANCODE or KEYEVENTF_KEYUP;
  end else
  begin
    if (VKey <> 255) then
    begin
      inpArr[4].ki.wVk := VKey;
      inpArr[4].ki.dwFlags := KEYEVENTF_KEYUP;
    end
    else
    begin
      inpArr[4].ki.wScan := Ord(Ch);
      inpArr[4].ki.dwFlags := KEYEVENTF_UNICODE or KEYEVENTF_KEYUP;
    end;
  end;

  inpArr[5].Itype := INPUT_KEYBOARD;
  inpArr[5].ki.wVk := SCAKey3;
  inpArr[5].ki.dwFlags := KEYEVENTF_KEYUP;

  inpArr[6].Itype := INPUT_KEYBOARD;
  inpArr[6].ki.wVk := SCAKey2;
  inpArr[6].ki.dwFlags := KEYEVENTF_KEYUP;

  inpArr[7].Itype := INPUT_KEYBOARD;
  inpArr[7].ki.wVk := SCAKey1;
  inpArr[7].ki.dwFlags := KEYEVENTF_KEYUP;

  SendInput(8,inpArr[0],sizeof(TInput));
end;

function TKeySender.ApplyDllSettings: Boolean;
var
  hw1, hw2, hwpar: HWND;
  cmppar: TComponent;
begin
  Result := True;
  if not CheckLib then
    exit;
  hw1 := 0;
  if FIAmExclusion then begin
    cmppar := self;
    while (cmppar<>nil) do begin
      cmppar := cmppar.Owner;
      if cmppar is TWinControl then begin
        hwpar := TWinControl(cmppar).Handle;
        while hwpar<>0 do begin
          hw1 := hwpar;
          hwpar := GetParent(hwpar);
        end;
        break;
      end;
    end;
  end;
  if assigned(FKeySenderLog) then
    hw2 := FKeySenderLog.Handle
  else
    hw2 := 0;
  try
    if FEnabled then
      ApplyData(hw1, hw2, trim(FTargetTitle), trim(FTargetExe), trim(FTargetClass), FExclusionTitles.CommaText,
        FExclusionExes.CommaText, FExclusionClasses.CommaText, FDebugEnabled, FCatchedKeyDown, FAfterKeyRedirection, FPaused,
        FCatchingKeyDownPaused)
    else // don't send keys to target
      ApplyData(hw1, hw2, '', '', '', '', '', '', FDebugEnabled, FCatchedKeyDown, FAfterKeyRedirection, FPaused,
        FCatchingKeyDownPaused)
  except
    on e: Exception do
    begin
      Result := False;
      deb('ApplyData('''+keydll+''') failed. Error: '+e.Message);
      log('ApplyData('''+keydll+''') failed. Error: '+e.Message);
    end;
  end;
end;

function TKeySender.CheckLib: boolean;
  function GetAddr(const nam:string): FARPROC;
  var
    lerr: integer;
  begin
    result := GetProcAddress(hlib,pchar(nam));
    if not assigned(result) then begin
      lerr := GetLastError;
      deb('GetProcAddress('''+nam+''') failed. Code '+lerr.ToString+': '+SysErrorMessage(lerr));
      log('GetProcAddress('''+nam+''') failed. Code '+lerr.ToString+': '+SysErrorMessage(lerr));
      if lerr = 127 then
      begin
        deb(keydll+' mismatch');
        log(keydll+' mismatch');
      end;
    end;
  end;
var
  lerr: integer;
begin
  if FLibChecked then
    exit (FLibLoaded);
  FLibChecked := true;
  FLibLoaded := false;
  hlib := LoadLibrary(keydll);
  if hlib=0 then begin
    lerr := GetLastError;
    deb('LoadLibrary('''+keydll+''') failed. Code '+lerr.ToString+': '+SysErrorMessage(lerr));
    log('LoadLibrary('''+keydll+''') failed. Code '+lerr.ToString+': '+SysErrorMessage(lerr));
    exit (FLibLoaded);
  end;
  InstallHook := GetAddr('InstallHook');
  if not assigned(InstallHook) then
    exit (FLibLoaded);
  UninstallHook := GetAddr('UninstallHook');
  if not assigned(UninstallHook) then
    exit (FLibLoaded);
  ApplyData := GetAddr('ApplyData2');
  if not assigned(ApplyData) then
    exit (FLibLoaded);
  FLibLoaded := true;
  deb('lib loaded');
  exit (FLibLoaded);
end;

constructor TKeySender.Create(AOwner: TComponent);
begin
  deb('TKeySender.Create');
  inherited;
  FEnabled := false;
  FLibChecked := false;
  FLibLoaded := false;
  FHookInstalled := false;
  FExclusionExes := TStringList.Create;
  FExclusionExes.CaseSensitive := false;
  FExclusionExes.Duplicates := dupIgnore;
  FExclusionExes.Sorted := true;
  FExclusionExes.StrictDelimiter := True;
  FExclusionTitles := TStringList.Create;
  FExclusionTitles.CaseSensitive := false;
  FExclusionTitles.Duplicates := dupIgnore;
  FExclusionTitles.Sorted := true;
  FExclusionTitles.StrictDelimiter := True;
  FExclusionClasses := TStringList.Create;
  FExclusionClasses.CaseSensitive := false;
  FExclusionClasses.Duplicates := dupIgnore;
  FExclusionClasses.Sorted := true;
  FExclusionClasses.StrictDelimiter := True;
  FExclusionClasses.Add('DV2ControlHost'); // windows 7
  FExclusionClasses.Add('Windows.UI.Core.CoreWindow'); // windows 10
  deb('TKeySender.Create E');
end;

procedure TKeySender.deb(const s: string);
begin
  if FDebugEnabled then
    OutputDebugString(PWideChar(s));
end;

destructor TKeySender.Destroy;
begin
  deb('TKeySender.Destroy');
  FExclusionExes.Free;
  FExclusionTitles.Free;
  FExclusionClasses.Free;
  deb('TKeySender.Destroy 1');
  inherited;
  deb('TKeySender.Destroy E');
end;

procedure TKeySender.DoBeforeSendKeys(Keys: string; var Cancel: Boolean);
begin
  if Assigned(FBeforeSendKeys) then
    FBeforeSendKeys(Keys, Cancel);
end;

procedure TKeySender.DoAfterSendKeys(Keys: string);
begin
  if Assigned(FAfterSendKeys) then
    FAfterSendKeys(Keys);
end;

function TKeySender.GetTargetHandle: HWND;
type
  TEnumWindowsParam = record
    Handle: HWND;
    TargetExeFileName: String;
  end;
  //PHWnd = ^HWND;
  PEnumWindowsParam = ^TEnumWindowsParam;
  chararr = array[0..255] of char;
var
  ProcTitle, ProcTitle1, ProcClass {, wndtitle}: chararr;
  trgHwnd : HWND;
  Param: TEnumWindowsParam;

  function NextWindow(wnd:Thandle; Param: PEnumWindowsParam):boolean;  stdcall;
  {This is the callback function which is called by EnumWindows procedure
   for each top-level window.  Return "true" to keep retrieving, return
   "false" to stop EnumWindows  from calling}
  var
    //title: array[0..255] of char;
    fileName: array[0..255] of char;
    //IsVisible: BOOL;
    IsOwned: Boolean;
    //IsAppWindow: Boolean;
    //IsFullScreen: BOOL;
    pid: DWORD;
    hproc: THandle;
    //IsEnabled: BOOL;
  begin
    Result := True;//carry on enumerating

    IsOwned := GetWindow(wnd, GW_OWNER)<>0;
    if IsOwned then
      exit;

    {IsAppWindow := GetWindowLongPtr(wnd, GWL_STYLE) and WS_THICKFRAME <> 0;
    if not IsAppWindow then
      exit;}

    {getwindowtext(wnd,title,256);
    if (title = '')  then
      exit;}

    GetWindowThreadProcessID(wnd, pid);
    hproc := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, False, pid);
    FileName := '';
    GetModuleFileNameEx(hproc,0,FileName,256);
    CloseHandle(hproc);

    if FileName = '' then
      exit;
    if ExtractFileName(FileName).ToLower = Param.TargetExeFileName.ToLower then
    begin
      Param^.Handle := wnd;
      Result := False;
    end;
  end;

begin
  //Result := 0;
  trgHwnd := 0;

  StrLCopy(ProcClass, PChar(FTargetClass), 255);

  if FTargetTitle <> '' then
  begin
    StrLCopy(ProcTitle, PChar(FTargetTitle), 255);
    trgHwnd := FindWindow(nil,ProcTitle);
    if trgHwnd = 0 then
    begin
      StrLCopy(ProcTitle1, PChar('*' + FTargetTitle), 255);
      trgHwnd := FindWindow(nil,ProcTitle1);
    end;
  end;

  if trgHwnd = 0 then
  begin
    if FTargetClass <> '' then
    begin
      StrLCopy(ProcClass, PChar(FTargetClass), 255);
      trgHwnd := FindWindow(ProcClass, nil);
    end;
  end;

  if trgHwnd = 0 then
  begin
    // get handle of running exe window
    Param.Handle := trgHwnd;
    Param.TargetExeFileName := FTargetExe;
    enumwindows(@nextwindow,lparam(@param)); {pass the address of trgHwnd as parameter}
    trgHwnd := Param.Handle;
  end;

  //if trgHwnd = 0 then
  //  raise Exception.Create('Target application is not running!');

  Result := trgHwnd;
end;

procedure TKeySender.PushKeys(Keys: string; ShiftOn, ControlOn, AltOn: Boolean);
var
  i, j : Integer;
  ch {, SCA_Ch} : Char;
  SubStr: String;
  //BrCnt: Integer; // Bracket count
  //BrOpen: String; // Open bracket
  //BrClose: String; // Close bracket
  OldShiftOn, OldControlOn, OldAltOn, ShiftChActive, CtrlChActive, AltChActive: Boolean;
  arrKeyStr: TArray<System.string>;
begin
  ShiftChActive := False;
  CtrlChActive := False;
  AltChActive := False;
  i := 1;
  while i <= Keys.Length do
  begin
    Ch := Keys[i];
    {SCA_Ch := #0;
    if (Ch = '+') or (Ch = '^') or (Ch = '%') then
    begin
      if (Ch = '+') and not ShiftChActive then
      begin
        SCA_Ch := ch;
        ShiftChActive := True;
      end;
      if (Ch = '^') and not CtrlChActive then
      begin
        SCA_Ch := ch;
        CtrlChActive := True;
      end;
      if (Ch = '%') and not AltChActive then
      begin
        SCA_Ch := ch;
        AltChActive := True;
      end;

      if SCA_Ch <> #0 then
      begin
        inc(i);
        continue;
      end;  // Actual SCA Character
    end; // '+^%'

    if (ch = '(') and (ShiftChActive or CtrlChActive or AltChActive) then
    begin
      inc(i);
      SubStr := '';
      BrCnt := 1;
      while (i <= Length(Keys)) And (BrCnt <> 0) do
      begin
        ch := Keys[i];
        if ch <> ')' Then
          SubStr := SubStr + ch
        else
          dec(BrCnt);
        if ch = '(' Then
          inc(BrCnt);
        inc(i);
      end;

      // Turn Shift Ctrl Alt on
      OldShiftOn := ShiftOn;
      OldControlOn := ControlOn;
      OldAltOn := AltOn;
      try
        if (ShiftChActive) and not ShiftOn Then
          ShiftOn := True;
        if (CtrlChActive) and not ControlOn Then
          ControlOn := True;
        if (AltChActive) and not AltOn Then
          AltOn := True;
        // Push the keys
        PushKeys(SubStr, ShiftOn, ControlOn, AltOn)
      finally
        ShiftChActive := False;
        CtrlChActive := False;
        AltChActive := False;
        ShiftOn := OldShiftOn;
        ControlOn := OldShiftOn;
        AltOn := OldAltOn;
      end;
      continue;
    end;}

    case ch of
     //'{', '~': // Function keys, Enter
     '{': // Function keys, Enter
      begin
        if ch = '{' then
        begin
          inc(i);
          SubStr := '';
          while (i <= Length(Keys)) And (Keys[i] <> '}') do
          begin
            SubStr := SubStr + Keys[i];
            inc(i);
          end;
        end;
        arrKeyStr := SubStr.Split(['-'],4, TStringSplitOptions.ExcludeEmpty);
        for j := 0 to Length(arrKeyStr) - 2 do
        begin
          if arrKeyStr[j].ToUpper =  'ALT' then
            AltChActive := True;
          if arrKeyStr[j].ToUpper =  'CTRL' then
            CtrlChActive := True;
          if arrKeyStr[j].ToUpper =  'SHIFT' then
            ShiftChActive := True;
        end;
        SubStr := arrKeyStr[Length(arrKeyStr) - 1];

        //else
        //if ch = '~' then
         // SubStr := 'ENTER';

        OldShiftOn := ShiftOn;
        OldControlOn := ControlOn;
        OldAltOn := AltOn;
        try
          if (ShiftChActive) and not ShiftOn Then
            ShiftOn := True;
          if (CtrlChActive) and not ControlOn Then
            ControlOn := True;
          if (AltChActive) and not AltOn Then
            AltOn := True;
          PushFnKey(SubStr, ShiftOn, ControlOn, AltOn);
        finally
          ShiftChActive := False;
          CtrlChActive := False;
          AltChActive := False;
          ShiftOn := OldShiftOn;
          ControlOn := OldControlOn;
          AltOn := OldAltOn;
        end;
      end;
      else
      begin
        ch := Keys[i];
        OldShiftOn := ShiftOn;
        OldControlOn := ControlOn;
        OldAltOn := AltOn;
        try
          if (ShiftChActive) and not ShiftOn Then
            ShiftOn := True;
          if (CtrlChActive) and not ControlOn Then
            ControlOn := True;
          if (AltChActive) and not AltOn Then
            AltOn := True;

          SendKey(ch, ShiftOn, ControlOn, AltOn);
        finally
          ShiftChActive := False;
          CtrlChActive := False;
          AltChActive := False;
          ShiftOn := OldShiftOn;
          ControlOn := OldControlOn;
          AltOn := OldAltOn;
        end;
      end;
    end;

    inc(i);
  end; // while
end;

procedure TKeySender.SendKeys(Keys: String);
var
  TtrgHwnd, InitHwnd: HWND;
  Cancel: Boolean;
begin
  TtrgHwnd := GetTargetHandle;

  if TtrgHwnd = 0 then
    raise Exception.Create('Target application is not running!');

  Cancel := False;
  DoBeforeSendKeys(Keys, Cancel);

  if not Cancel then
  begin
    InitHwnd := GetForegroundWindow;
    SetForegroundWindow(TtrgHwnd);

    try
      PushKeys(Keys, False, False, False);
      DoAfterSendKeys(Keys);
      Sleep(200);
    finally
       SetForegroundWindow(InitHwnd);
    end;
  end;
end;

procedure TKeySender.Loaded;
begin
  deb('Loaded'+' csDesigning='+(csDesigning in ComponentState).ToString+' csLoading='+(csLoading in ComponentState).ToString);
  inherited;
  deb('Loaded E'+' csDesigning='+(csDesigning in ComponentState).ToString+' csLoading='+(csLoading in ComponentState).ToString);
  SetEnabledRuntime;
  SetCatchingKeyDownEnabledRuntime;
end;

procedure TKeySender.log(const s: string);
begin
  if assigned(FKeySenderLog) then
    FKeySenderLog.log(s);
end;

procedure TKeySender.PushFnKey(KeyCode: String; ShiftOn, ControlOn, AltOn: Boolean);
var
  NumPushes: Longint;
  i: Integer;
  FnKey: String;
  OrigCode: String;
  VKey: Byte;
begin
  // Work out which function key to push and how many times
  If Pos(' ', KeyCode) > 0 Then
  begin
    // We're doing this multpile times
    // Find the rightmost space. I do this because I want to allow people to
    // output multiple words with spaces in them. eg '{Seven times 7}'
    i := Length(KeyCode);
    While (KeyCode[i] <> ' ') do
      dec(i);
    OrigCode := Copy(KeyCode, 1, i - 1);
    FnKey := UpperCase(OrigCode);
    // This may fail if not a number, in which case NumPushes will be 1
    try
      NumPushes := StrToInt(Copy(KeyCode, i + 1, Length(KeyCode)));
    except
      NumPushes := 1;
    end;
  end
  Else
  begin
    // Once only
    NumPushes := 1;
    FnKey := UpperCase(KeyCode);
    OrigCode := KeyCode;
  End;

  // Function Keys
  Vkey := 0;
  if (FnKey = 'BACKSPACE') or (FnKey = 'BS') or (FnKey = 'BKSP') // Backspace
  then
    Vkey := VK_BACK
  else if (FnKey = 'BREAK') or (FnKey = 'CANCEL') // Break = ^C
  then
    Vkey := VK_CANCEL
  else if (FnKey = 'CAPSLOCK') or (FnKey = 'CAPS') // Capslock
  then
    Vkey := VK_CAPITAL
  else if (FnKey = 'DELETE') or (FnKey = 'DEL') // Delete
  then
    Vkey := VK_DELETE
  else if FnKey = 'DOWN' // Down Arrow
  then
    Vkey := VK_DOWN
  else if FnKey = 'END' // End
  then
    Vkey := VK_END
  else if (FnKey = 'ENTER') or (FnKey = 'RETURN') // Enter = ^M
  then
    Vkey := VK_RETURN
  else if (FnKey = 'ESCAPE') or (FnKey = 'ESC') // Escape = ^[
  then
    Vkey := VK_ESCAPE
  else if (FnKey = 'FORMFEED') or (FnKey = 'FF') // Form feed = ^L
  then
    Vkey := VK_CLEAR
  else if FnKey = 'HELP' // Help
  then
    Vkey := VK_HELP
  else if FnKey = 'HOME' // Home
  then
    Vkey := VK_HOME
  else if (FnKey = 'INSERT') or (FnKey = 'INS') // Insert
  then
    Vkey := VK_INSERT
  else if FnKey = 'LEFT' // Left Arrow
  then
    Vkey := VK_LEFT
  else if (FnKey = 'NEWLINE') or (FnKey = 'NL')
  // New line = Carraige return & Line Feed = ^M^J
  then
    Vkey := VK_RETURN
  else if FnKey = 'NUMLOCK' // Numeric Lock
  then
    Vkey := VK_NUMLOCK
  else if (FnKey = 'PGDN') or (FnKey = 'PAGEDOWN') or (FnKey = 'NEXT')
  // Page Down
  then
    Vkey := VK_NEXT
  else if (FnKey = 'PGUP') or (FnKey = 'PAGEUP') or (FnKey = 'PRIOR') // Page Up
  then
    Vkey := VK_PRIOR
  else if (FnKey = 'PRINTSCREEN') or (FnKey = 'PRTSC') // Print Screen
  then
    Vkey := VK_PRINT
  else if FnKey = 'RIGHT' // Right Arrow
  then
    Vkey := VK_RIGHT
  else if (FnKey = 'SCROLLLOCK') or (FnKey = 'SCRLK') // Scroll lock
  then
    Vkey := VK_SCROLL
  else if (FnKey = 'TAB') // Tab = ^I
  then
    Vkey := VK_TAB
  else if (FnKey = 'UP') // Up Arrow
  then
    Vkey := VK_UP
  else if (FnKey = 'F1') // F1
  then
    Vkey := VK_F1
  else if (FnKey = 'F2') // F2
  then
    Vkey := VK_F2
  else if (FnKey = 'F3') // F3
  then
    Vkey := VK_F3
  else if (FnKey = 'F4') // F4
  then
    Vkey := VK_F4
  else if (FnKey = 'F5') // F5
  then
    Vkey := VK_F5
  else if (FnKey = 'F6') // F6
  then
    Vkey := VK_F6
  else if (FnKey = 'F7') // F7
  then
    Vkey := VK_F7
  else if (FnKey = 'F8') // F8
  then
    Vkey := VK_F8
  else if (FnKey = 'F9') // F9
  then
    Vkey := VK_F9
  else if (FnKey = 'F10') // F10
  then
    Vkey := VK_F10
  else if (FnKey = 'F11') // F11
  then
    Vkey := VK_F11
  else if (FnKey = 'F12') // F12
  then
    Vkey := VK_F12
  else if (FnKey = 'F13') // F13
  then
    Vkey := VK_F13
  else if (FnKey = 'F14') // F14
  then
    Vkey := VK_F14
  else if (FnKey = 'F15') // F15
  then
    Vkey := VK_F15
  else if (FnKey = 'F16') // F16
  then
    Vkey := VK_F16
  else if (FnKey = 'F17') // F17
  then
    Vkey := VK_F17
  else if (FnKey = 'F18') // F18
  then
    Vkey := VK_F18
  else if (FnKey = 'F19') // F19
  then
    Vkey := VK_F19
  else if (FnKey = 'F20') // F20
  then
    Vkey := VK_F20
  else if (FnKey = 'F21') // F21
  then
    Vkey := VK_F21
  else if (FnKey = 'F22') // F22
  then
    Vkey := VK_F22
  else if (FnKey = 'F23') // F23
  then
    Vkey := VK_F23
  else if (FnKey = 'F24') // F24
  then
    Vkey := VK_F24
  else if (FnKey = 'NUMPAD0') // Numeric Keypad 0
  then
    Vkey := VK_NUMPAD0
  else if (FnKey = 'NUMPAD1') // Numeric Keypad 1
  then
    Vkey := VK_NUMPAD1
  else if (FnKey = 'NUMPAD2') // Numeric Keypad 2
  then
    Vkey := VK_NUMPAD2
  else if (FnKey = 'NUMPAD3') // Numeric Keypad 3
  then
    Vkey := VK_NUMPAD3
  else if (FnKey = 'NUMPAD4') // Numeric Keypad 4
  then
    Vkey := VK_NUMPAD4
  else if (FnKey = 'NUMPAD5') // Numeric Keypad 5
  then
    Vkey := VK_NUMPAD5
  else if (FnKey = 'NUMPAD6') // Numeric Keypad 6
  then
    Vkey := VK_NUMPAD6
  else if (FnKey = 'NUMPAD7') // Numeric Keypad 7
  then
    Vkey := VK_NUMPAD7
  else if (FnKey = 'NUMPAD8') // Numeric Keypad 8
  then
    Vkey := VK_NUMPAD8
  else if (FnKey = 'NUMPAD9') // Numeric Keypad 9
  then
    Vkey := VK_NUMPAD9
  else if (FnKey = 'NUMPADMULTIPLY') or (FnKey = 'NUMPAD*')
  // Numeric Keypad Multiply
  then
    Vkey := VK_MULTIPLY
  else if (FnKey = 'NUMPADADD') or (FnKey = 'NUMPAD+') // Numeric Keypad +
  then
    Vkey := VK_ADD
  else if (FnKey = 'NUMPADSUBTRACT') or (FnKey = 'NUMPAD-') // Numeric Keypad -
  then
    Vkey := VK_SUBTRACT
  else if (FnKey = 'NUMPADDECIMAL') or (FnKey = 'NUMPAD.') // Numeric Keypad .
  then
    Vkey := VK_DECIMAL
  else if (FnKey = 'NUMPADDIVIDE') or (FnKey = 'NUMPAD/') // Numeric Keypad /
  then
    Vkey := VK_DIVIDE;

  if Vkey <> 0 then
  begin
    For i := 1 To NumPushes do
      SendFNKey(Vkey, ShiftOn, ControlOn, AltOn);
  end
  else
  if FnKey = 'SLEEP' then
    Sleep(NumPushes * 1000)
  else if FnKey = 'BEEP' then
  begin
    For i := 1 To NumPushes do
      MessageBeep(0);
  end
  else
    For i := 1 To NumPushes do
      PushKeys(OrigCode, ShiftOn, ControlOn, AltOn);
end;

procedure TKeySender.SendFnKey(VKey: Byte; Shift: Boolean; Control: Boolean; Alt: Boolean);
var
  //ckl: HKL;
  ScanCode: Word;
begin
  //ckl := GetKeyboardLayout(0);

  ScanCode := 0;
  //if Alt and not (vkey in  [0,255]) then
  //begin
  //  ScanCode := MapVirtualKeyEx(vkey, MAPVK_VK_TO_VSC, ckl);
  //end;

  if (not Shift) and (not Control) and (not Alt) then
    SendKeyPress(vkey)
  else if Shift and (not Control) and (not Alt) then
    SendSCAKeyPress(VK_SHIFT, Vkey, ScanCode)
  else if (not Shift) and Control and (not Alt) then
    SendSCAKeyPress(VK_CONTROL, Vkey, ScanCode)
  else if not Shift and (not Control) and Alt then
    SendSCAKeyPress(VK_MENU, Vkey, ScanCode)
  else if Shift and Control and (not Alt) then
    Send2SCAKeyPress(VK_CONTROL, VK_SHIFT, Vkey, ScanCode)
  else if Shift and not Control and Alt then
    Send2SCAKeyPress(VK_MENU, VK_SHIFT, Vkey, ScanCode)
  else if (not Shift) and Control and Alt then
    Send2SCAKeyPress(VK_CONTROL, VK_MENU, Vkey, ScanCode)
  else if Shift and Control and Alt then
    Send3SCAKeyPress(VK_SHIFT, VK_CONTROL, VK_MENU, Vkey, ScanCode);

  Sleep(50);
end;

procedure TKeySender.SendKey(ch: Char; ShiftOn: Boolean; ControlOn: Boolean; AltOn: Boolean);
var
  ckl: HKL;
  VKeyShiftState: Short;
  ScanCode: Word;
  VKey, ShiftState: Byte;
  Shift, Control, Alt {, Hankaku}: Boolean;
begin
  ckl := GetKeyboardLayout(0);
  VKeyShiftState := VkKeyScanEx(ch, ckl);
  if VKeyShiftState <> -1 then
  begin
    VKey := VKeyShiftState and 255;
    ShiftState := VKeyShiftState shr 8;
  end
  else
  begin
    VKey := 255;
    ShiftState := 0;
  end;

//  if CapsLockOn then
//    if ((VKey >= 65) and (Vkey <= 90)) or ((Ord(Ch) >= 192) and (Ord(Ch) < 700)) then
//    begin
//      if (ShiftState mod 2) = 0 then
//        ShiftState := ShiftState + 1
//      else
//        ShiftState := ShiftState - 1;
//      Shift := not Shift;
//    end;
  Shift := ((ShiftState and 1) = 1);
  Control := (ShiftState and 2) = 2;
  Alt := (ShiftState and 4) = 4;
  //Hankaku := (ShiftState and 8) = 8;

  if ShiftOn  then
    Shift := not Shift;

  if not Control and ControlOn  then
    Control := True;

  if not Alt and AltOn then
    Alt := True;

  if Control or Alt then
    Shift := ShiftOn;

  ScanCode := 0;
  if Alt and not (vkey in  [0,255]) then
  begin
    ScanCode := MapVirtualKeyEx(vkey, MAPVK_VK_TO_VSC, ckl);
  end;

  if (not Shift) and (not Control) and (not Alt) then
    SendKeyPress(vkey, ch)
  else if Shift and (not Control) and (not Alt) then
    SendSCAKeyPress(VK_SHIFT, Vkey, ScanCode, Ch)
    //SendSCAKeyPressWithScanCode(VK_SHIFT, Vkey, ScanCode, ch)
  else if (not Shift) and Control and (not Alt) then
    SendSCAKeyPress(VK_CONTROL, Vkey, ScanCode, Ch)
    //SendSCAKeyPressWithScanCode(VK_CONTROL, Vkey, ScanCode, ch)
  else if not Shift and (not Control) and Alt then
    SendSCAKeyPress(VK_MENU, Vkey, ScanCode, ch)
    //SendSCAKeyPressWithScanCode(VK_MENU, Vkey, ScanCode, ch)
  else if Shift and Control and (not Alt) then
    Send2SCAKeyPress(VK_CONTROL, VK_SHIFT, Vkey, ScanCode, ch)
  else if Shift and not Control and Alt then
    Send2SCAKeyPress(VK_MENU, VK_SHIFT, Vkey, ScanCode, ch)
  else if (not Shift) and Control and Alt then
    Send2SCAKeyPress(VK_CONTROL, VK_MENU, Vkey, ScanCode, ch)
  else if Shift and Control and Alt then
    Send3SCAKeyPress(VK_SHIFT, VK_CONTROL, VK_MENU, Vkey, ScanCode, ch);

  if Control or Alt then
    Sleep(50);
end;

procedure TKeySender.SetEnabled(const Value: boolean);
begin
  deb('SetEnabled FEnabled='+FEnabled.ToString+' Value='+Value.ToString);
  if FEnabled = Value then
    exit;
  FEnabled := Value;
  SetEnabledRuntime;
end;

procedure TKeySender.SetEnabledRuntime;
begin
  if (csDesigning in ComponentState) or (csLoading in ComponentState) then
    exit;
  if not CheckLib then begin
    FEnabled := false;
    exit;
  end;
  if FEnabled then
  begin
    if (trim(FTargetExe)='') and (trim(FTargetTitle)='') and (trim(FTargetClass)='') then begin
      log('Cannot enable: target not specified');
      FEnabled := false;
      exit;
    end;

    if Assigned(FCatchedKeyDown) then
      log('Catched Key Down handler is specified');
    if Assigned(FAfterKeyRedirection) then
      log('After key redirection handler is specified');

    if not ApplyDllSettings then
    begin
      log('Dll Settings couldn''t be applied. Enable failed');
      FEnabled := false;
      exit;
    end;

    if not FHookInstalled then
      FHookInstalled := InstallHook;

    if not FHookInstalled then begin
      log('Hook couldn''t be installed. Enable failed');
      FEnabled := false;
      exit;
    end;
  end
  else
  begin
    if FPaused then
      FPaused := False;
    if not FCatchingKeyDownEnabled then
    begin
      UninstallHook;
      FHookInstalled := False;
    end;
  end;
end;

procedure TKeySender.SetCatchingKeyDownEnabled(const Value: Boolean);
begin
  deb('SetCatchingKeyDownEnabled FCatchingKeyDownEnabled='+FCatchingKeyDownEnabled.ToString+' Value='+Value.ToString);
  if FCatchingKeyDownEnabled = Value then
    exit;
  FCatchingKeyDownEnabled := Value;
  SetCatchingKeyDownEnabledRuntime;
end;

procedure TKeySender.SetCatchingKeyDownEnabledRuntime;
begin
  if (csDesigning in ComponentState) or (csLoading in ComponentState) then
    exit;

  if not CheckLib then begin
    FEnabled := false;
    exit;
  end;

  if FCatchingKeyDownEnabled then
  begin
    if Assigned(FCatchedKeyDown) then
      log('Catched Key Down handler is specified');
    if Assigned(FAfterKeyRedirection) then
      log('After key redirection handler is specified');

    if not ApplyDllSettings then
    begin
      log('Dll Settings couldn''t be applied. Enable CatchKeyDown failed');
      FCatchingKeyDownEnabled := false;
      exit;
    end;

    if not FHookInstalled then
      FHookInstalled := InstallHook;

    if not FHookInstalled then begin
      log('Hook couldn''t be installed. Enable CatchKeyDown failed');
      CatchingKeyDownEnabled := false;
      exit;
    end;
  end
  else
  begin
    if FCatchingKeyDownPaused then
      FCatchingKeyDownPaused := False;
    if not FEnabled then
    begin
      UninstallHook;
      FHookInstalled := False;
    end;
  end;
end;

procedure TKeySender.SetCatchingKeyDownPaused(const Value: Boolean);
begin
  FCatchingKeyDownPaused := Value;

  if (csDesigning in ComponentState) or (csLoading in ComponentState) then
    exit;

  if FCatchingKeyDownEnabled and Assigned(FCatchingKeyDownPausedChange) then
    FCatchingKeyDownPausedChange(FCatchingKeyDownPaused);

  if FCatchingKeyDownPaused and not FCatchingKeyDownEnabled then
  begin
    log('Catching Key Down is not enabled. Catching Key Down couldn''t be paused');
    FCatchingKeyDownPaused := false;
    exit;
  end;

  if FLibLoaded then
  begin
    if not ApplyDllSettings then
    begin
      log('Dll Settings couldn''t be applied.');
      Enabled := false;
      exit;
    end;
  end;
end;

procedure TKeySender.SetDebugEnabled(const Value: boolean);
begin
  FDebugEnabled := Value;
  if (csDesigning in ComponentState) or (csLoading in ComponentState) then
    exit;

  if FLibLoaded then
  begin
    if not ApplyDllSettings then
    begin
      log('Dll Settings couldn''t be applied.');
      Enabled := false;
      exit;
    end;
  end;
end;

procedure AssignStringList(ststo, stsfr: TStringList);
var
  i: integer;
  s: string;
begin
  ststo.Clear;
  for i := 0 to stsfr.Count-1 do begin
    s := trim(stsfr[i]);
    if s<>'' then
      ststo.Add(s);
  end;
end;

procedure TKeySender.SetExclusionClasses(const Value: TStringList);
begin
  AssignStringList(FExclusionClasses,Value);
end;

procedure TKeySender.SetExclusionExes(const Value: TStringList);
begin
  AssignStringList(FExclusionExes,Value);
end;

procedure TKeySender.SetExclusionTitles(const Value: TStringList);
begin
  AssignStringList(FExclusionTitles,Value);
end;

procedure TKeySender.SetPaused(const Value: Boolean);
begin
  FPaused := Value;

  if (csDesigning in ComponentState) or (csLoading in ComponentState) then
    exit;

  if FEnabled and Assigned(FPausedChange) then
    FPausedChange(FPaused);

  if FPaused and not FEnabled then
  begin
    log('Key sender is not enabled. Key sender couldn''t be paused');
    FPaused := false;
    exit;
  end;

  if FLibLoaded then
  begin
    if not ApplyDllSettings then
    begin
      log('Dll Settings couldn''t be applied.');
      Enabled := false;
      exit;
    end;
  end;
end;

procedure TKeySender.SetTargetClass(const Value: string);
var
  NewValue: string;
begin
  NewValue := trim(Value);
  if FTargetClass <> trim(Value) then
  begin
    FTargetClass := trim(Value);
  end;
end;

procedure TKeySender.SetTargetExe(const Value: string);
var
  NewValue: string;
begin
  NewValue := trim(Value);
  if FTargetExe <> NewValue  then
  begin
    FTargetExe := NewValue;
  end;
end;

procedure TKeySender.SetTargetTitle(const Value: string);
var
  NewValue: string;
begin
  NewValue := trim(Value);
  if FTargetTitle <>  NewValue then
  begin
    FTargetTitle := NewValue;
  end;
end;

procedure Register;
begin
  RegisterComponents('Samples',[TKeySender]);
end;

end.
