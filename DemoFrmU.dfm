object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 338
  ClientWidth = 890
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object cbEnabled: TCheckBox
    Left = 8
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Enabled'
    TabOrder = 0
    OnClick = cbEnabledClick
  end
  object edToBeSendTxt: TEdit
    Left = 80
    Top = 264
    Width = 297
    Height = 21
    TabOrder = 1
    TextHint = 'Please enter the text to be send  to target'
  end
  object btnSend: TButton
    Left = 390
    Top = 262
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = btnSendClick
  end
  object KeySenderLog1: TKeySenderLog
    Left = 175
    Top = 8
    Width = 658
    Height = 227
    LogEnabled = True
    TabOrder = 3
  end
  object btnClearLog: TButton
    Left = 488
    Top = 262
    Width = 75
    Height = 25
    Caption = 'ClearLog'
    TabOrder = 4
    OnClick = btnClearLogClick
  end
  object cbEnableCatchKeyDown: TCheckBox
    Left = 8
    Top = 96
    Width = 160
    Height = 17
    Caption = 'Catching Key Down Enabled'
    TabOrder = 5
    OnClick = cbEnableCatchKeyDownClick
  end
  object cbPaused: TCheckBox
    Left = 9
    Top = 136
    Width = 97
    Height = 17
    Caption = 'Paused'
    TabOrder = 6
    OnClick = cbPausedClick
  end
  object cbCatchingKeyDownPaused: TCheckBox
    Left = 9
    Top = 176
    Width = 160
    Height = 17
    Caption = 'Catching Key Down Paused'
    TabOrder = 7
    OnClick = cbCatchingKeyDownPausedClick
  end
  object KeySender1: TKeySender
    Enabled = True
    Paused = False
    TargetClass = 'Notepad'
    ExclusionClasses.Strings = (
      'DV2ControlHost'
      'Windows.UI.Core.CoreWindow')
    IAmExclusion = True
    KeySenderLog = KeySenderLog1
    DebugEnabled = True
    BeforeSendKeys = KeySender1BeforeSendKeys
    CatchedKeyDown = KeySender1CatchedKeyDown
    AfterKeyRedirection = KeySender1AfterKeyRedirection
    CatchingKeyDownEnabled = False
    CatchingKeyDownPaused = False
    PausedChange = KeySender1PausedChange
    CatchingKeyDownPausedChange = KeySender1CatchingKeyDownPausedChange
    Left = 108
    Top = 163
  end
end
