{
Sample TargetClass=Notepad
}

unit DemoFrmU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, KeySenderLogU, KeySenderU, Vcl.AppEvnts;

type
  TForm1 = class(TForm)
    KeySender1: TKeySender;
    cbEnabled: TCheckBox;
    edToBeSendTxt: TEdit;
    btnSend: TButton;
    KeySenderLog1: TKeySenderLog;
    btnClearLog: TButton;
    cbEnableCatchKeyDown: TCheckBox;
    cbPaused: TCheckBox;
    cbCatchingKeyDownPaused: TCheckBox;
    procedure btnClearLogClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbEnabledClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure KeySender1BeforeSendKeys(Keys: string; var Cancel: Boolean);
    procedure KeySender1AfterKeyRedirection(WM: NativeUInt; Key: PKBDLLHOOKSTRUCT; ch: Char; ShiftDown, CtrlDown,
      AltDown: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure KeySender1CatchedKeyDown(WM: NativeUInt; Key: PKBDLLHOOKSTRUCT; ch: Char; var CancelKeyRedirection, EatKeyUp,
      RedirectToKeySenderHost: Boolean; ShiftDown, CtrlDown, AltDown: Boolean; KeyOriginatorType: TKeyOriginatorType);
    procedure cbEnableCatchKeyDownClick(Sender: TObject);
    procedure cbPausedClick(Sender: TObject);
    procedure cbCatchingKeyDownPausedClick(Sender: TObject);
    procedure KeySender1PausedChange(Paused: Boolean);
    procedure KeySender1CatchingKeyDownPausedChange(Paused: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.ApplicationEvents1Exception(Sender: TObject; E: Exception);
begin
   ShowMessage(e.Message);
end;

procedure TForm1.btnClearLogClick(Sender: TObject);
begin
  KeySenderLog1.Clear ;
end;

procedure TForm1.btnSendClick(Sender: TObject);
begin
  KeySender1.SendKeys(edToBeSendTxt.Text);
end;

procedure TForm1.cbCatchingKeyDownPausedClick(Sender: TObject);
begin
  KeySender1.CatchingKeyDownPaused := cbCatchingKeyDownPaused.Checked;
  cbCatchingKeyDownPaused.Checked := KeySender1.CatchingKeyDownPaused;
end;

procedure TForm1.cbEnableCatchKeyDownClick(Sender: TObject);
begin
  KeySender1.CatchingKeyDownEnabled := cbEnableCatchKeyDown.Checked;
  cbEnableCatchKeyDown.Checked := KeySender1.CatchingKeyDownEnabled;
end;

procedure TForm1.cbEnabledClick(Sender: TObject);
begin
  KeySender1.Enabled := cbEnabled.Checked;
  cbEnabled.Checked := KeySender1.Enabled;
end;

procedure TForm1.cbPausedClick(Sender: TObject);
begin
  KeySender1.Paused := cbPaused.Checked;
  cbPaused.Checked := KeySender1.Paused;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if KeySender1.Enabled then
  begin
    KeySender1.Enabled := False;
    KeySender1.Free;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  cbEnabled.Checked := KeySender1.Enabled;
  cbEnableCatchKeyDown.Checked := KeySender1.CatchingKeyDownEnabled;

  Application.OnException := ApplicationEvents1Exception;
end;

procedure TForm1.KeySender1AfterKeyRedirection(WM: NativeUInt; Key: PKBDLLHOOKSTRUCT; ch: Char; ShiftDown, CtrlDown,
  AltDown: Boolean);
begin
  KeySenderlog1.log(format('Key is redirected. wm: %d, vkcode: %d scancode: %d, char: %s, Shift: %s, Ctrl: %s, Alt: %s',
    [wm, key.vkCode, key.scanCode, ch, BoolToStr(ShiftDown, True), BoolToStr(CtrlDown, True), BoolToStr(AltDown, True)]));
end;

procedure TForm1.KeySender1BeforeSendKeys(Keys: string; var Cancel: Boolean);
begin
  KeySenderlog1.log(format('Text will be send to target application. text: %s', [Keys]));
//  if (Keys = 'A') or (Keys = 'b') then
//    Cancel := True;
end;

procedure TForm1.KeySender1CatchedKeyDown(WM: NativeUInt; Key: PKBDLLHOOKSTRUCT; ch: Char; var CancelKeyRedirection, EatKeyUp,
  RedirectToKeySenderHost: Boolean; ShiftDown, CtrlDown, AltDown: Boolean; KeyOriginatorType: TKeyOriginatorType);
var
  strKeyOrigType: string;
begin
  if KeyOriginatorType = Host then
    strKeyOrigType := 'Host'
  else if KeyOriginatorType = Target then
    strKeyOrigType := 'Target'
  else
    strKeyOrigType := 'Other';

  KeySenderlog1.log(format('Key Down is catched. wm: %d, vkcode: %d scancode: %d, char: %s, Shift: %s, Ctrl: %s, Alt: %s, Key Originator Type: %s',
    [wm, key.vkCode, key.scanCode, ch, BoolToStr(ShiftDown, True), BoolToStr(CtrlDown, True), BoolToStr(AltDown, True), strKeyOrigType]));
  //ShowMessage('Key is catched!');
  //if (Key.vkCode = 65) and not CtrlDown and not AltDown then // A
//  if (ch = 'A') and not CtrlDown and not AltDown then // A
//  begin
//    KeySenderlog1.log(format('Redirection of key will be canceled. wm: %d, vkcode: %d scancode: %d, char: %s', [wm, key.vkCode, key.scanCode, ch]));
//    CancelKeyRedirection := True;
//  end;
//  if (ch = 'b') and not CtrlDown and not AltDown then // b
//  begin
//    KeySenderlog1.log(format('Key will be eaten up. wm: %d, vkcode: %d scancode: %d, char: %s', [wm, key.vkCode, key.scanCode, ch]));
//    EatKeyUp := True;
//  end;
//  if (ch = 'c') and not CtrlDown and not AltDown then // c
//  begin
//    KeySenderlog1.log(format('Key will be redirected to host app. wm: %d, vkcode: %d scancode: %d, char: %s', [wm, key.vkCode, key.scanCode, ch]));
//    RedirectToKeySenderHost := True;
//  end;
end;

procedure TForm1.KeySender1CatchingKeyDownPausedChange(Paused: Boolean);
begin
  KeySenderlog1.log(format('Catching Key down paused is changed. Paused: %s', [BoolToStr(Paused, True)]));
end;

procedure TForm1.KeySender1PausedChange(Paused: Boolean);
begin
  KeySenderlog1.log(format('Key sender paused is changed. Paused: %s', [BoolToStr(Paused, True)]));
end;

end.
