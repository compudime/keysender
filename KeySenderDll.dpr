library KeySenderDll;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  Winapi.Windows,
  Winapi.Messages,
  PsApi,
  System.Generics.Collections,
  strutils
  ,Mahrisutl
  ;

{$R *.res}

type
  TWnd = (wnd_target, wnd_excl, wnd_ord, wnd_notarget);
  PKBDLLHOOKSTRUCT = ^KBDLLHOOKSTRUCT;
  KBDLLHOOKSTRUCT = record
    vkCode: DWORD;
    scanCode: DWORD;
    flags: DWORD;
    time: DWORD;
    dwExtraInfo: ULONG_PTR;
  end;

  TKeyOriginatorType = (Host, Target, Other);

  // TCatchedKeyDownEvent and TAfterKeyRedirectionEvent syntaxes must be same as the types with same name in KeySenderU unit.
  TCatchedKeyDownEvent = procedure (WM: WPARAM; Key: PKBDLLHOOKSTRUCT; ch: Char; var CancelKeyRedirection, EatKeyUp,
    RedirectToKeySenderHost: Boolean; ShiftDown, CtrlDown, AltDown: Boolean; KeyOriginatorType: TKeyOriginatorType) of object;
  TAfterKeyRedirectionEvent = procedure (WM: WPARAM; Key: PKBDLLHOOKSTRUCT; ch: Char; ShiftDown, CtrlDown, AltDown: Boolean) of object;

  TKeyRedirectionFlags = record
    CancelKeyRedirection: Boolean;
    EatKeyUp: Boolean;
    RedirectToKeySenderHost: Boolean;
  end;

const
  sMutex = '45e05617-5867-419d-b705-de4b03de2a80';

var
  cnt:integer=0;
  hMutex: THandle=0;
  g_hInstance: HINST = 0;
  g_hKeyboard_LL: HHOOK = 0;
  hwTarget: HWND = 0;
  hwLog: HWND = 0;
  hwLogPar: HWND = 0;
  hwMain: HWND = 0;
  hwExclusions: TList<HWND>;
  hwOrdinal: TList<HWND>;
  namTargetClass: string;
  namTargetTit: string;
  namTargetExe: string;
  namExclClasses: TStringList;
  namExclTits: TStringList;
  namExclExes: TStringList;
  created: boolean = false;
  FDebugEnabled: boolean = false;
  FPaused, FCatchedKeyDownPaused: Boolean;
  //vkDown: TList<DWORD>;
  vkDown: TDictionary<DWORD, TKeyRedirectionFlags>;

  WM_PLEASEALLOWFOREGROUND: cardinal = 0;
  WM_KEYSENDERLOGMSG: cardinal = 0;
  CatchedKeyDown: TCatchedKeyDownEvent;
  afterKeyRedirection: TAfterKeyRedirectionEvent;
  ckl: Cardinal;

procedure deb(const s:string);
begin
  if FDebugEnabled then
    OutputDebugString(PWideChar(s));
end;

function tostr(r:UINT_PTR):string; overload;
begin
  result:=IntToHex(r,2);
end;
procedure log(const s:string; force:boolean=false);
var
  iforce: integer;
  s1: string;
  a: ATOM;
begin
  if hwLog<>0 then begin
    if force then iforce:=1 else iforce:=0;
    s1 := copy(s,1,254);
    a := AddAtom(Pchar(s1));
    PostMessage(hwLog, WM_KEYSENDERLOGMSG, iforce, a);
  end;
  deb(s);
end;

procedure CreateObj;
begin
  if created then
    exit;
  //g_hInstance := 0;
  g_hKeyboard_LL := 0;
  hwTarget:= 0;
  hwExclusions := TList<HWND>.Create;
  hwOrdinal := TList<HWND>.Create;
  namExclClasses := TStringList.Create;
  namExclClasses.CaseSensitive := false;
  namExclClasses.Duplicates := dupIgnore;
  namExclClasses.Sorted := true;
  namExclClasses.StrictDelimiter := True;
  namExclTits := TStringList.Create;
  namExclTits.CaseSensitive := false;
  namExclTits.Duplicates := dupIgnore;
  namExclTits.Sorted := true;
  namExclTits.StrictDelimiter := True;
  namExclExes := TStringList.Create;
  namExclExes.CaseSensitive := false;
  namExclExes.Duplicates := dupIgnore;
  namExclExes.Sorted := true;
  namExclExes.StrictDelimiter := True;
  //vkDown := TList<DWORD>.Create;
  vkDown := TDictionary<DWORD, TKeyRedirectionFlags>.Create;
  WM_PLEASEALLOWFOREGROUND := RegisterWindowMessage('KeySender_PleaseAllowForeground');
  WM_KEYSENDERLOGMSG := RegisterWindowMessage('KeySender_LogMessage');
  created := true;
end;

procedure DestroyObj;
begin
  if not created then
    exit;
  created := false;
  if g_hKeyboard_LL<>0 then
    UnhookWindowsHookEx(g_hKeyboard_LL);
  g_hKeyboard_LL := 0;
  FreeAndNil(hwExclusions);
  FreeAndNil(hwOrdinal);
  FreeAndNil(namExclClasses);
  FreeAndNil(namExclTits);
  FreeAndNil(namExclExes);
  FreeAndNil(vkDown);
  namTargetTit := '';
  namTargetExe := '';
end;

procedure InitList(wnd:TWnd);
var
  lst: TList<HWND>;
begin
  if wnd=wnd_excl then lst:=hwExclusions
  else lst:=hwOrdinal;
  if lst.Count>50 then begin
    if wnd=wnd_excl then begin
      lst.Clear;
      if hwMain<>0 then begin
        lst.Add(hwMain);
        if hwLogPar<>0 then
          lst.Add(hwLogPar);
      end;
    end
    else
      lst.Clear;
  end;
end;

function InList(hw:HWND; wnd:TWnd): boolean;
var
  lst: TList<HWND>;
  i: integer;
  hwpar: HWND;
begin
  InitList(wnd);
  if wnd=wnd_excl then lst:=hwExclusions
  else lst:=hwOrdinal;
  for i := 0 to lst.Count-1 do begin
    hwpar := hw;
    while hwpar<>0 do begin
      if hwpar=lst[i] then
        exit(true);
      hwpar := GetParent(hwpar);
    end;
  end;
  result := false;
end;

function IsTargetChild(hw:HWND): boolean;
var
  hwpar: HWND;
begin
  if not IsWindow(hwTarget) then
    exit(false);
  hwpar := hw;
  while hwpar<>0 do begin
    if hwpar=hwTarget then
      exit(true);
    hwpar := GetParent(hwpar);
  end;
  result := false;
end;

type
  PInfo=^TInfo;
  TInfo=record
    pidown, pidchi: DWORD;
  end;
  TQueryFullProcessImageName = function(hProcess: Thandle; dwFlags: DWORD; lpExeName: PWideChar;
    nSize: PDWORD): BOOL; stdcall;

// Possibly change thread PID, if child has different
function EnumChildWindowsProc(hw:HWND; lp:LPARAM): BOOL; stdcall;
var
  pid:DWORD;
  pinf: PInfo;
begin
  pid:=0;
  pinf:=PInfo(lp);
  GetWindowThreadProcessId(hw,@pid);
  if (pid<>0) and (pid<>pinf^.pidown) then
    pinf^.pidchi:=pid;
  result:=true;
end;

function GetExePath(hw:HWND): string;
var
  ph: THandle;
  inf: TInfo;
  QueryFullProcessImageName: TQueryFullProcessImageName;
  n: integer;
begin
  result := '';
  inf.pidown :=0;
  GetWindowThreadProcessId(hw,@inf.pidown);
  if inf.pidown<>0 then begin
    inf.pidchi:=inf.pidown;
    EnumChildWindows(hw,@EnumChildWindowsProc,LParam(@inf));
    ph := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,false,inf.pidchi);
    if ph<>0 then begin
      setlength(result,MAX_PATH+1);
      n:=GetModuleFileNameEx(ph,0,PWideChar(result),MAX_PATH);
      setlength(result,n);
      if n=0 then begin
        @QueryFullProcessImageName := GetProcAddress(GetModuleHandle('kernel32'), 'QueryFullProcessImageNameW');
        if assigned(QueryFullProcessImageName) then begin
          n:=MAX_PATH;
          setlength(result,MAX_PATH+1);
          result[1]:=#0;
          QueryFullProcessImageName(ph, 0, PWideChar(result), @n);
          setlength(result,n);
        end;
      end;
      CloseHandle(ph);
    end;
  end;
end;

function EnumWindowsProc(hw:HWND; lp:LPARAM): BOOL; stdcall;
var
  s: string;
  n: integer;
  rect: TRect;
begin
  if not IsWindowVisible(hw) then
    exit(true);
  fillchar(rect,sizeof(rect),0);
  GetWindowRect(hw,rect);
  if (rect.Height=0) and (rect.Width=0) then // [TApplication] window in Delphi executables
    exit(true);
  if namTargetClass<>'' then begin
    setlength(s,MAX_PATH);
    n := GetClassName (hw,PWideChar(s),MAX_PATH);
    setlength(s,n);
    if SameText(s,namTargetClass) then begin
      hwTarget := hw;
      log('Found target: class name "'+s+'"');
      exit(false);
    end;
  end;
  if namTargetTit<>'' then begin
    setlength(s,MAX_PATH);
    n := GetWindowText(hw,PWideChar(s),MAX_PATH);
    setlength(s,n);
    if SameText(s,namTargetTit) then begin
      hwTarget := hw;
      log('Found target: title "'+s+'"');
      exit(false);
    end;
  end;
  if namTargetExe<>'' then begin
    s := GetExePath(hw);
    if EndsText(namTargetExe,s) then begin
      hwTarget := hw;
      log('Found target: executable "'+s+'"');
      exit(false);
    end;
  end;
  result := true;
end;

procedure FindTarget;
begin
  if (hwTarget<>0) and IsWindow(hwTarget) then
    exit;
  hwTarget := 0;
  EnumWindows(@EnumWindowsProc,0);
end;

// Called for foregroud windows
function DetectType(hw:HWND): TWnd;
var
  s: string;
  i: integer;
  hwpar: HWND;
begin
  if (hwTarget<>0) and not IsWindow(hwTarget) then
    hwTarget := 0;
  if hw=hwTarget then
    exit(wnd_target);
  if IsTargetChild(hw) then
    exit(wnd_excl);
  if InList(hw,wnd_excl) then
    exit(wnd_excl);
  if InList(hw,wnd_ord) then
    exit(wnd_ord);
  FindTarget;
  if hwTarget=0 then
    exit(wnd_notarget);
  if hw=hwTarget then
    exit(wnd_target);
  if IsTargetChild(hw) then
    exit(wnd_excl);
  if namExclClasses.Count>0 then begin
    hwpar := hw;
    while hwpar<>0 do begin
      setlength(s,MAX_PATH);
      setlength(s,GetClassName (hwpar,PWideChar(s),MAX_PATH));
      if (s<>'') and (namExclClasses.IndexOf(s)>=0) then begin
        hwExclusions.Add(hw);
        if hwpar<>hw then
          hwExclusions.Add(hwpar);
        log('Found exclusion: class name "'+s+'"');
        exit(wnd_excl);
      end;
      hwpar := GetParent(hwpar);
    end;
  end;
  if namExclTits.Count>0 then begin
    hwpar := hw;
    while hwpar<>0 do begin
      setlength(s,MAX_PATH);
      setlength(s,GetWindowText(hwpar,PWideChar(s),MAX_PATH));
      if (s<>'') and (namExclTits.IndexOf(s)>=0) then begin
        hwExclusions.Add(hw);
        if hwpar<>hw then
          hwExclusions.Add(hwpar);
        log('Found exclusion: title "'+s+'"');
        exit(wnd_excl);
      end;
      hwpar := GetParent(hwpar);
    end;
  end;
  if namExclExes.Count>0 then begin
    hwpar := hw;
    while hwpar<>0 do begin
      s := GetExePath(hwpar);
      for i := 0 to namExclExes.Count-1 do begin
        if EndsText(namExclExes[i],s) then begin
          hwExclusions.Add(hw);
          if hwpar<>hw then
            hwExclusions.Add(hwpar);
          log('Found exclusion: executable name "'+s+'"');
          exit(wnd_excl);
        end;
      end;
      hwpar := GetParent(hwpar);
    end;
  end;
  hwOrdinal.Add(hw);
  result := wnd_ord;
end;

function logKey(wp:WParam; vkCode: DWORD; KeyDirectionFlags: TKeyRedirectionFlags): boolean;
var
  //ind: integer;
  KeyExists: Boolean;
begin
  result := true;
  //ind := vkDown.IndexOf(vkCode);
  KeyExists := vkDown.ContainsKey(vkCode);
  case wp of
    WM_KEYDOWN, WM_SYSKEYDOWN: begin
      //if ind>=0 then
      if KeyExists then
        result := false
      else
        vkDown.Add(vkCode, KeyDirectionFlags);
    end;
    WM_KEYUP, WM_SYSKEYUP: begin
      //if ind>=0 then
      if KeyExists then
        //vkDown.Delete(ind);
        vkDown.Remove(vkCode);
    end;
  end;
end;

function tostrVM(wp:WParam): string;
begin
  case wp of
    WM_KEYDOWN: result:='KeyDown';
    WM_KEYUP: result:='KeyUp';
    WM_SYSKEYDOWN: result:='SysKeyDown';
    WM_SYSKEYUP: result:='SysKeyUp';
    else result := '($'+IntToHex(wp,2)+')';
  end;
end;
function tostrVK(scanCode,flags,vkCode:DWORD): string;
var
  lp: LParam;
begin
  lp := (scanCode shl 16)+((flags and 1) shl 24)+1;
  setlength(result,1024);
  setlength(result,GetKeyNameText(lp,PWideChar(result),1024));
  if result='' then
    result := '(Unknown: scanCode=$'+IntToHex(scanCode,2)+' vkCode=$'+IntToHex(vkCode,2)+')';
end;

procedure debwnd(hw:HWND; const pref:string='');
var
  stit, scls, sexe: string;
  hwpar: HWND;
begin
  setlength(stit,MAX_PATH);
  setlength(stit,GetWindowText(hw,PChar(stit),MAX_PATH));
  setlength(scls,MAX_PATH);
  setlength(scls,GetClassName(hw,PChar(scls),MAX_PATH));
  sexe := GetExePath(hw);
  hwpar := GetParent(hw);
  deb(pref+' hw='+tostr(hw)+' par='+tostr(hwpar)+' tit='+stit+' cls='+scls+' exe='+sexe+';');
  if hwpar<>0 then
    debwnd(hwpar,'parent');
end;

function LowLevelKeyboardProc(nCode:integer; wp:WPARAM; lp:LPARAM): LRESULT; stdcall;
var
  ordsucc: boolean;
  ch: Char;
  ShiftDown, CtrlDown, AltDown: Boolean;
  KeyOriginatorType: TKeyOriginatorType;
  KeyDirectionFlags: TKeyRedirectionFlags;
  hwRedirectionTarget: HWND;

  function checkforg(const pref:string): boolean;
  begin
    result := hwRedirectionTarget=GetForegroundWindow;
    ordsucc := result;
    deb(pref+' target=foreground: '+result.ToString)
  end;

  function GetPressedCharacter(pkey: PKBDLLHOOKSTRUCT; var Shift: Boolean; var Ctrl: Boolean; var Alt: Boolean): Char;
  var
    flags: Cardinal;
    kbdstat: TKeyboardState;
    Buffer: WideString;
    pBuffer: PWideChar;
    BufSize: Integer;
    toUnicExResult: Integer;
  begin
    Result := #0;

    Shift := GetKeyState(VK_SHIFT) < 0;
    Ctrl := GetKeyState(VK_CONTROL) < 0;
    Alt := GetKeyState(VK_MENU) < 0;

    if (not (pkey.vkCode in [VK_SHIFT, VK_CONTROL, VK_MENU, VK_LSHIFT, VK_RSHIFT, VK_LCONTROL, VK_RCONTROL, VK_LMENU, VK_RMENU]))
      and GetKeyboardState(kbdstat) then
    begin
      begin
        kbdstat[VK_CAPITAL] := GetKeyState(VK_CAPITAL);
        SetLength(Buffer, 2);
        Buffer := #0#0;
        pBuffer := Addr(Buffer[1]);
        BufSize := 2;
        Flags := 4; // no menu selected and don't change keybd state
        toUnicExResult := ToUnicodeEx(pkey.vkCode, pkey.scanCode, kbdstat, pBuffer, BufSize, flags, ckl);
        if toUnicExResult = 1 then
          Result := Buffer[1];
      end;
    end;
  end;

var
  hw, hwtit: HWND;
  wnd: TWnd;
  curpid, hwpid, tarpid, curpidw: DWORD;
  bf,bft,bat,bsh: BOOL;
  lerr,lerrf,lerrft: integer;
  smsg, stit, stit1: string;
  pkey: PKBDLLHOOKSTRUCT;
  timot, timot0, timot1: DWORD;
  sw: integer;
begin
  deb('LowLevelKeyboardProc nCode='+nCode.ToString+' wp='+wp.ToString);
  if (nCode=HC_ACTION) and not FPaused then begin
    //bf := false;
    inc(cnt);
    hw := GetForegroundWindow;
    wnd := DetectType(hw);
    pkey := PKBDLLHOOKSTRUCT(lp);
    deb('hw='+tostr(hw)+' wnd='+Tenum.ToStr(wnd)
      +' vkCode='+pkey.vkCode.ToHexString+' scanCode='+pkey.scanCode.ToHexString
      +' flags='+pkey.flags.ToHexString+' extra='+tostr(pkey.dwExtraInfo));
    //
    with KeyDirectionFlags do
    begin
      CancelKeyRedirection := False;
      EatKeyUp := False;
      RedirectToKeySenderHost := False;
    end;

    if wp=WM_KEYUP then
      vkDown.TryGetValue(pkey.scanCode, KeyDirectionFlags);

    Ch := #0;

    if ((not FCatchedKeyDownPaused) and Assigned(CatchedKeyDown) and (wp=WM_KEYDOWN) and not vkDown.ContainsKey(pkey.scanCode)) or
       (Assigned(afterKeyRedirection) and (wnd=wnd_ord) and not KeyDirectionFlags.CancelKeyRedirection and
        not KeyDirectionFlags.EatKeyUp and not ((wp=WM_KEYUP) and (pkey.scanCode=$21D))) then
    // CatchedKeyDown will be fired or afterKeyRedirection will be fired after key direction
    begin
      ch := GetPressedCharacter(pkey, ShiftDown, CtrlDown, AltDown);
      if (hw = hwMain) or IsChild(hwMain, hw) then
        KeyOriginatorType := Host
      else if wnd = wnd_target then
        KeyOriginatorType := Target
      else
        KeyOriginatorType := Other;

      if (not FCatchedKeyDownPaused) and Assigned(CatchedKeyDown) and (wp=WM_KEYDOWN) then
        CatchedKeyDown(wp, pkey, ch, KeyDirectionFlags.CancelKeyRedirection, KeyDirectionFlags.EatKeyUp,
          KeyDirectionFlags.RedirectToKeySenderHost, ShiftDown, CtrlDown, AltDown, KeyOriginatorType);
    end;
    //
    if not ((wp=WM_KEYUP) and (pkey.scanCode=$21D)) then begin // Strange behavior, possibly my kbd driver. If real press, we activated already on key down
      debwnd(hw,'forg');
      if (wnd=wnd_ord) and not KeyDirectionFlags.CancelKeyRedirection and not KeyDirectionFlags.EatKeyUp then repeat
        if KeyDirectionFlags.RedirectToKeySenderHost then
          hwRedirectionTarget := hwMain
        else
          hwRedirectionTarget := hwTarget;

        // First attempt - ShowWindow
        if IsIconic(hwRedirectionTarget) then sw := SW_RESTORE
        else if IsZoomed(hwRedirectionTarget) then sw := SW_SHOWMAXIMIZED
        else sw := SW_SHOWNORMAL;
        ShowWindow(hwRedirectionTarget,sw);
        if checkforg('Show') then break;
        // Second - attach process and try foreground
        curpid := GetCurrentProcessId;
        PostMessage(hw, WM_PLEASEALLOWFOREGROUND, curpid, 0);
        GetWindowThreadProcessId(hw,@hwpid);
        GetWindowThreadProcessId(hwRedirectionTarget,@tarpid);
        GetWindowThreadProcessId(hwLog,@curpidw);
        deb('curpid='+curpid.ToString+' curpidw='+curpidw.ToString+' tarpid='+tarpid.ToString+' hwpid='+hwpid.ToString);
        bat := false;
        if curpid=hwpid then begin
          bft :=BringWindowToTop(hwRedirectionTarget);
          lerrft:=GetLastError;
          bf:=SetForegroundWindow(hwRedirectionTarget);
          lerrf:=GetLastError;
          ShowWindow(hwRedirectionTarget,sw);
        end
        else begin
          bat := AttachThreadInput(curpid, hwpid, true);
          lerr := GetLastError;
          bft :=BringWindowToTop(hwRedirectionTarget);
          lerrft:=GetLastError;
          bf:=SetForegroundWindow(hwRedirectionTarget);
          lerrf:=GetLastError;
          bsh := ShowWindow(hwRedirectionTarget,sw);
          //SetFocus(hwRedirectionTarget);
          AttachThreadInput(curpid, hwpid, false);
        end;
        deb('Thread'+' bf='+booltostr(bf,true)+' lerrf='+lerrf.ToString
          +' bft='+booltostr(bft,true)+' lerrft='+lerrft.ToString
          +' bat='+booltostr(bat,true)+' lerr='+lerr.ToString+' bsh='+bsh.ToString);
        if checkforg('Attach') then break;
        // Third attempt - foreground timeout
        timot := 0;
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timot, 0);
        deb('timot='+timot.ToHexString);
        timot0 := 0;
        bat:=SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, @timot0, SPIF_SENDCHANGE);
        timot1 := 0;
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timot1, 0);
        deb('timot1='+timot1.ToHexString);
        bft :=BringWindowToTop(hwRedirectionTarget);
        lerrft:=GetLastError;
        bf:=SetForegroundWindow(hwRedirectionTarget);
        lerrf:=GetLastError;
        deb('Timeou'+' bf='+booltostr(bf,true)+' lerrf='+lerrf.ToString
          +' bft='+booltostr(bft,true)+' lerrft='+lerrft.ToString
          +' bat='+booltostr(bat,true)+' lerr='+lerr.ToString);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, @timot0, SPIF_SENDCHANGE);
        timot1 := 0;
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timot1, 0);
        deb('timot1='+timot1.ToHexString);
        ShowWindow(hwRedirectionTarget,sw);
        if checkforg('Timeout') then break;
        // Fourth attempt - minimime me (removed: not help)
        //if not IsIconic(hwMain) then
        //  ShowWindow(hwMain,SW_HIDE);
        //if not IsIconic(hwLog) then
        //  ShowWindow(hwLog,SW_HIDE);
        //SendMessage(hwMain,WM_CLOSE,0,0);
        //SendMessage(hwLog,WM_CLOSE,0,0);
        //BringWindowToTop(hwRedirectionTarget);
        //SetForegroundWindow(hwRedirectionTarget);
        //ShowWindow(hwRedirectionTarget,sw);
        if checkforg('MinimizeMe') then break;
        // Last - minimize and restore
        ShowWindow(hwRedirectionTarget,SW_MINIMIZE);
        ShowWindow(hwRedirectionTarget,SW_RESTORE);
        lerrf := GetLastError;
        checkforg('Minimize');
      until true;
    end;
    if logKey(wp, pkey.scanCode, KeyDirectionFlags) then begin
      smsg := tostrVM(wp);
      smsg := smsg+' '+tostrVK(pkey.scanCode,pkey.flags,pkey.vkCode);
      if KeyDirectionFlags.EatKeyUp then
      begin
        smsg := smsg + ' is eaten up';
      end else
      begin
        //if (wnd=wnd_ord) and ordsucc then hwtit:=hwTarget else hwtit:=hw;
        if (wnd=wnd_ord) and ordsucc then hwtit:=hwRedirectionTarget else hwtit:=hw;
        setlength(stit, MAX_PATH);
        setlength(stit, GetWindowText(hwtit,PWideChar(stit),MAX_PATH));
        case wnd of
          wnd_target: smsg := smsg + ' - Already active target: '+stit;
          wnd_excl: smsg := smsg + ' - Exclusion, left active: '+stit;
          wnd_ord: begin
            if KeyDirectionFlags.CancelKeyRedirection then
            begin
              smsg := smsg + ' - Redirection canceled: '+stit;
            end
            else
            if ordsucc then
            begin
              smsg := smsg +' - Activated target: '+stit;
              if Assigned(afterKeyRedirection) then
                afterKeyRedirection(wp, pkey, ch, ShiftDown, CtrlDown, AltDown);
            end
            else begin
              setlength(stit1, MAX_PATH);
              //setlength(stit1, GetWindowText(hwTarget,PWideChar(stit1),MAX_PATH));
              setlength(stit1, GetWindowText(hwRedirectionTarget,PWideChar(stit1),MAX_PATH));
              smsg := ' - Could not activate target "'+stit1+'", error '+lerrf.ToString+'.'
                +' Input left to "'+stit+'"';
            end;
          end;
          wnd_notarget: smsg := smsg + ' - No target found, left active: '+stit;
        end;
      end;
      log(smsg);
    end;
  end;
  if FPaused or not KeyDirectionFlags.EatKeyUp then
    result := CallNextHookEx(g_hKeyboard_LL,nCode,wp,lp)
  else
    result := -1; // nonzero value to get other processes not to process message
end;


function InstallHook: boolean;
var
  lerr: integer;
begin
  if g_hKeyboard_LL<>0 then begin
    UnhookWindowsHookEx(g_hKeyboard_LL);
    g_hKeyboard_LL := 0;
    if hMutex<>0 then begin
      ReleaseMutex(hMutex);
      hMutex := 0;
    end;
  end;
  hMutex := CreateMutex(nil, True, sMutex);
  lerr := GetLastError;
  if hMutex=0 then begin
    log('Could not create hook mutex. Error '+lerr.ToString);
    exit(false);
  end;
  if lerr=ERROR_ALREADY_EXISTS then begin
    log('Hook not installed - already running on another instance');
    ReleaseMutex(hMutex);
    CloseHandle(hMutex);
    hMutex := 0;
    exit(false);
  end;
  g_hKeyboard_LL:=SetWindowsHookEx(WH_KEYBOARD_LL, @LowLevelKeyboardProc, g_hInstance, 0);
  lerr := GetLastError;
  result := g_hKeyboard_LL<>0;;
  if not result then
    log('Hook not installed. SetWindowsHookEx failed with error '+lerr.ToString,true)
  else
    log('Hook installed',true);
  hwTarget := 0;
end;

function UninstallHook: BOOL;
var
  lerr: integer;
begin
  if g_hKeyboard_LL=0 then
    exit(true);
  result:=UnhookWindowsHookEx(g_hKeyboard_LL);
  lerr := GetLastError;
  if not result then
    log('Hook not freed. UnhookWindowsHookEx failed with error '+lerr.ToString,true)
  else
    log('Hook uninstalled',true);
  g_hKeyboard_LL := 0;
  if hMutex<>0 then begin
    ReleaseMutex(hMutex);
    CloseHandle(hMutex);
    hMutex := 0;
  end;
end;

procedure ApplyData2(hwMainWnd, hwLogWnd: HWND; const namTargTit, namTargExe, namTargClass, namExclTit, namExclExe, namExclClass: string;
  DebugEnabled: boolean; CatchedKeyDownproc: TCatchedKeyDownEvent; afterKeyRedirectionproc: TAfterKeyRedirectionEvent;
  Paused, CatchedKeyDownPaused: Boolean);
var
  hwpar: HWND;
begin
  FDebugEnabled := DebugEnabled;
  hwTarget:= 0;
  hwLog := hwLogWnd;
  hwLogPar := hwLog;
  hwpar := hwLog;
  while hwpar<>0 do begin
    hwLogPar := hwpar;
    hwpar := GetParent(hwpar);
  end;
  hwMain := hwMainWnd;
  hwExclusions.Clear;
  if hwMain<>0 then begin
    hwExclusions.Add(hwMain);
    if hwLogPar<>0 then
      hwExclusions.Add(hwLogPar);
  end;
  hwOrdinal.Clear;
  namTargetClass := namTargClass;
  UniqueString(namTargetClass);
  namTargetTit := namTargTit;
  UniqueString(namTargetTit);
  namTargetExe := namTargExe;
  UniqueString(namTargetExe);
  namExclClasses.Clear;
  namExclClasses.CommaText := namExclClass;
  namExclTits.Clear;
  namExclTits.CommaText := namExclTit;
  namExclExes.Clear;
  namExclExes.CommaText := namExclExe;
  deb('namExclExe='+namExclExe);
  CatchedKeyDown := CatchedKeyDownproc;
  afterKeyRedirection := afterKeyRedirectionproc;
  FPaused := Paused;
  FCatchedKeyDownPaused := CatchedKeyDownPaused;
  deb('Paused='+BoolToStr(Fpaused)+',CatchedKeyDownPaused='+BoolToStr(FCatchedKeyDownPaused));
  ckl := GetKeyboardLayout(0);
  log('New data applied',true);
end;

exports
  InstallHook,
  UninstallHook,
  ApplyData2;

procedure DllMain(reason: integer);
begin
  deb('DllMain'+' reason='+reason.ToString+' g_hInstance='+g_hInstance.ToHexString+' gmh='+tostr(GetModuleHandle(nil))+' HInstance='+tostr(HInstance)+' created='+created.ToString);
  case reason of
    DLL_PROCESS_DETACH: DestroyObj;
    DLL_THREAD_ATTACH: ;
  end;
  //g_hInstance := GetModuleHandle(nil);
end;

begin
  DllProc := @DllMain;
  g_hInstance := HInstance;
  //g_hInstance := GetModuleHandle(nil);
  DllProc(DLL_PROCESS_ATTACH);
  CreateObj;
end.

