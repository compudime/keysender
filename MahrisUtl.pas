{
  Some useful utilities created by Maris Janis Vasilevskis (Mahris)
  Free for use, copy, modify
}

unit MahrisUtl;

interface

type
  Tenum = class
  public
    class function ToInt<T>(const EnumValue: T): Integer;
    class function ToStr<T>(EnumValue: T): string;  // ToString hides TObject
  end;
type
  TBooleanHelper = record helper for Boolean
  public
    function ToInteger: Integer; inline;
    function ToString(UseBoolStrs: boolean = true): string; overload; inline;
  end;

type
  TPointerHelper = record helper for Pointer
  public
    function ToString: string; overload;
  end;
procedure deb(const s:string);

implementation

uses System.Typinfo, System.SysUtils, Winapi.Windows;

procedure deb(const s:string);
begin
  OutputDebugString(PWideChar(s));
end;


{ Tenum }

class function TEnum.ToInt<T>(const EnumValue: T): Integer;
begin
  Result := 0;
  if sizeOf(EnumValue)<=4 then
    Move(EnumValue, Result, sizeOf(EnumValue))
  else
    raise Exception.Create('Not enumeration type');
end;

class function Tenum.ToStr<T>(EnumValue: T): string;
var
  pti: PTypeInfo;
  ptd : PTypeData;
  good: boolean;
  int: integer;
begin
  good := false;
  pti := PTypeInfo(TypeInfo(T));
  int := ToInt(EnumValue);
  if assigned(pti) then begin
    if pti^.Kind<>tkEnumeration then
      raise Exception.Create('Not enumeration type');
    ptd := GetTypeData(pti);
    if assigned(ptd) then begin // probably, always
      if (int>=ptd^.MinValue) and (int<=ptd^.MaxValue) then begin
        Result := GetEnumName(pti, int);
        good := true;
      end;
    end;
  end;
  if not good then begin
    case sizeof(EnumValue) of
      1: result := '('+PByte(@EnumValue)^.ToString+')';
      2: result := '('+PWord(@EnumValue)^.ToString+')';
      else result := '('+PInteger(@EnumValue)^.ToString+')';
    end;
  end;
end;

{ TBooleanHelper }

function TBooleanHelper.ToInteger: Integer;
begin
  Result := Integer(Self);
end;

function TBooleanHelper.ToString(UseBoolStrs: boolean): string;
begin
  Result := BoolToStr(Self, Boolean(UseBoolStrs));
end;

{ TPointerHelper }

function TPointerHelper.ToString: string;
begin
  result := Format('%p',[Self]);
end;

end.
