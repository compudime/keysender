unit KeySenderLogU;

interface

uses System.Classes, System.SysUtils, Vcl.Controls, Vcl.StdCtrls, Winapi.Messages;

type

  TKeySenderLog = class(TCustomMemo)
  private
    FLogEnabled: boolean;
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure log(const s:string);
  published
    property LogEnabled: boolean read FLogEnabled write FLogEnabled;
    property Align;
    property Alignment;
    property Anchors;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property Font;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property TabOrder;
    property TabStop;
  end;

procedure Register;

implementation

uses Winapi.Windows;

var
  WM_KEYSENDERLOGMSG: cardinal = 0;

{ TKeySenderLog }

constructor TKeySenderLog.Create(AOwner: TComponent);
begin
  inherited;
  ScrollBars := ssVertical;
  ReadOnly := true;
  if not (csDesigning in ComponentState) then
    WM_KEYSENDERLOGMSG := RegisterWindowMessage('KeySender_LogMessage');
end;

procedure TKeySenderLog.log(const s: string);
begin
  Lines.Add(FormatDateTime('hh:nn:ss.zzz ',Now)+s);
  SendMessage(Handle, EM_LINESCROLL, 0,Lines.Count);
end;

procedure TKeySenderLog.WndProc(var Message: TMessage);
var
  s: string;
begin
  inherited;
  if Message.Msg=WM_KEYSENDERLOGMSG then begin
    if (Message.WParam<>0) or FLogEnabled then begin
      setlength(s,255);
      setlength(s,GetAtomName(Message.LParam,pchar(s),255));
      log(s);
    end;
    DeleteAtom(Message.LParam);
  end;
end;

procedure Register;
begin
  RegisterComponents('Samples',[TKeySenderLog]);
end;

end.
